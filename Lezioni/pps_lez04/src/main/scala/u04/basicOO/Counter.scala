package u04.basicOO

// A trait defines a type, it includes a contract and possibly an incomplete class
trait Counter {
  def increment(): Unit // Unit is the equivalent of "void"
  def getValue(): Int
}

// a class with no constructors, i.e., only default constructor
class CounterImpl extends Counter {

  private var value: Int = 0 // a mutable field

  // public is the default visibility, override is a keyword
  override def increment(): Unit = {
    this.value = this.value+1 // operator ++ on variables is too "imperative"..
  }

  override def getValue(): Int = {
    return this.value // return keyword not needed, but possible
  }
}

// a module, with a main
object UseCounter extends  App { // note an object could extend a class..
    val counter: Counter = new CounterImpl()  // "()" tipically always optional
    counter.increment()
    counter.increment()
    counter.increment()
    println("Result is "+counter.getValue()) // 3
}
