package u04.basicOO

object Matches extends App {

  class Pair[A,B](val x: A, val y: B)

  object Pair {
    def apply[A,B](x: A, y: B) = new Pair(x,y)
    def unapply[A,B](p: Pair[A,B]): Option[(A,B)] = Some((p.x,p.y))
  }

  object Circ { // extracting distance to 0 when greater than 1
    def unapply(p: Pair[Double,Double]): Option[Double] =
      Some(Math.sqrt(p.x*p.x+p.y*p.y)) filter (_>1)
  }

  class MyPair(override val x: Double, override val y: Double)
    extends Pair[Double,Double](x,y)

  def process(p: Pair[Double,Double]): String = p match {
    case p: MyPair => "MYPAIR"  // matching by type
    case Pair(0,0) => "Zero"    // matching a case class
    case Pair(_,0) => "X-axys"  // using unbounded parameters
    case Pair(0,1) | Pair (0,2) => "some along Y-axys" // or-case
    case Pair(x,y) if x==y => "X=Y"  // conditional case
    case pair @ Pair(x,y) if x<0 && y<0 => "SW"+pair // capturing variable
    case Circ(n) => "C"+n  // usign a custom extractor
    case _ => "?" // default case
  }

  println(process(Pair(0,0)))
  println(process(Pair(10,0)))
  println(process(Pair(0,2)))
  println(process(Pair(10,10)))
  println(process(Pair(-1,-2)))
  println(process(Pair(1,2)))
  println(process(new MyPair(0,0)))
  println(process(Pair(0.2,0.1)))

}
