package u03

object Currying extends App {

  // standard function with no currying
  def mult(x: Double, y: Double): Double = x*y

  // function with currying
  // curriedMult has actually type: Double => (Double => Double)
  def curriedMult(x: Double)(y: Double): Double = x*y

  // curriedMult can be partially applied!
  val twice: Double => Double = curriedMult(2)

  val curriedMultAsFunction: Double => Double => Double = x => y => x*y

  println(mult(10,2)) // 20
  println(curriedMult(10)(2)) // 20
  println(twice(10)) // 20
  println(curriedMultAsFunction(10)(2)) // 20
  println(curriedMultAsFunction) // u03.Currying$$$Lambda$7/1221555852@3d24753a
  println(curriedMultAsFunction(10)) // u03.Currying$$$Lambda$12/1468177767@3d24753a

}
