package u03

object Values extends App {

  // values can be associated to names
  // namely, non modifiable variables
  val v = 1

  // types are optional, they are often inferred
  val w: Int = 1

  // a procedure to print on console
  println(v)

  // Java-style concatenation, semi-colon is optional on CR
  println("result is " + v);

  // primitive types as expected
  val i: Int = 10 + 5 // as in Java, to be read +(10,5)
  val l: Long = 100000000000L // as in Java
  val d: Double = 5.4 * 1.3 // as in Java
  val f: Float = 3.0f // as in Java
  val b: Boolean = true && false // as in Java
  val s: String = "hello" concat " to all" // method as operator
  val n: String = null // null can be passed to "objects"

  println(i, l, d, f, b, s, n) // println has a var-arg

}
