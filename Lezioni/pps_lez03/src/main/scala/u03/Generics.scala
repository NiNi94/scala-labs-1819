package u03

object Generics extends App {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case (l1, Cons(h, t)) => Cons(h, append(l1, t))
      case _ => Nil()
    }
  }

  // Note "List." qualification
  println(List.sum(List.Cons(10, List.Cons(20, List.Cons(30, List.Nil()))))) // 60
  import List._
  println(append(Cons("10", Nil()), Cons("1", Cons("2", Nil())))) // "10","1","2"

  //Suggested functions for exercises:
  // def drop[A](l: List[A], n: Int): List[A]
  // def dropWhile[A](l: List[A])(f: A => Boolean): List[A]
  // def map[A,B](l: List[A])(f: A => B): List[B]
  // def flatMap[A,B](l: List[A])(f: A => List[B]): List[B]
}
