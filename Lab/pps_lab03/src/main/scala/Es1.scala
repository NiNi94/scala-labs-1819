import Sealed.{ILNode, IntList}

object TestType extends App {
  val int: Int = 3
  val long: Long = 1000000000000000L
  val double: Double = 2.3d
  val float: Float = 2.333f
  val boolean: Boolean = true
  val string: String = "Diocane"
  println(int, long, double, float, boolean, string)
}

object TestLambdas extends App {
  val f1: (Int, Int) => Int = (x: Int, y: Int) => x + y
  println("Versione ultra dichiarativa " + f1(3, 5))

  val f2: (Int, Int) => Boolean = (x, y) => x == y
  println("Versione meno dichiarativa " + f2(3, 2))

  val f3: (Int, Int) => String = _.toString + _.toString
  println("Versione molto meno dichiarativa " + f3(3, 5))

  val f4 = (x: Int, y: Int) => (x + y).toString()
  println("Versione inferita molto dichiarativa " + f4(2, 5))

  var f5: (Int, Int) => String = f4
  println("Assegnamento di lambda " + f5(3, 5))


  val g: (Int, Int,  (Int, Int) => Int) => Int = (a, b, f) => a + f(a, b)
  val prodotto: (Int, Int) => Int = (a :Int, b: Int) => a * b
  println("Chiamo g che accetta due valori e applica una funzione per fare qualcosa " + g(3, 5, prodotto))

  val h: Int => Int = x => {
    println("Sto facendo qualcosa dentro la lambda")
    x + 3
  }
  println("Ora chiamo una lambda (somma di 3) che ha all'interno una stampa " + h(3)) // calcola il valore per poter stampare quello che deve fare
}

object TestMatchCase extends App {
  val productOnlyPositive: (Int, Int) => Int = (x, y) => (x, y) match {
    case (x: Int, y: Int) if (x < 0 || y < 0) => throw new IllegalStateException("Solo numeri positivi")
    case (x: Int, y: Int) => x * y
  }

  try {
    println("Uso la lambda e metto dei valori negativi " + productOnlyPositive(-2, 3))
  } catch {
    case e => println(e)
  }

  //E' come se fosse uno switch sul valore 5
  val res: String = productOnlyPositive(2, 3) match {
    case 5 => "Diocane"
    case 3 => "Porco dio"
    case _ => "Puttana"
  }
  println("Chiamo il single value  " + res)
}

object TestFunctions extends App {
  def square(d: Double): Double = d * d
  println("Stampo il primo metodo che calcola il quadrato " + square(3))

  def factorial(x: Int): Int = x match {
    case 0|1 => 1
    case x if x > 1 => x * factorial(x - 1)
  }
  println("Chiamo il fattoriale di 5 " + factorial(5))


  val z: Double = "ciao" match {
    case "ciao" => 3.0
    case _ => 5.0
  }
  println("Chiamo la match su input = ciao " + z)

  def factorial2(x: Int):Int = {
    def _fact(n: Int, acc: Int): Int = n match {
      case 0 | 1 => acc
      case n if n > 1 => _fact(n - 1, n * acc)
    }
    _fact(x, 1)
  }
  println("Provo a usare il fattoriale ricorsivo tail di 5 " + factorial2(5))
}

object currying extends App {
  def mul(x: Int, y: Int): Int = x * y
  def curriedMul(x: Int)(y: Int): Int = x * y
  val twice: Int => Int = curriedMul(2)
  val curriedMultAsFunction: Int => Int => Int = x => y => x * y

  println("Twice: " + twice(30))
  println("Curried mul: " + curriedMul(10)(4))

}

object AlgebricData extends App {
  case class Point2D(x: Double, y: Double)
  val p1: Point2D = Point2D(20, 30)
  val p2: Point2D = Point2D(50, 30)

  def rotate(p: Point2D): Point2D = p match {
    case Point2D(x, y) => Point2D(y, -x)
  }
  def sum(p1: Point2D, p2: Point2D): Point2D = (p1, p2) match {
    case (Point2D(x1, y1), Point2D(x2, y2)) => Point2D(x1 + x2, y1 + y2)
  }
  def getX(p: Point2D): Double = p match {
    case Point2D(x, _) => x
    case _ => throw new IllegalArgumentException
  }

  println("Ho creato p1: " + p1 + "e ora stampo la sua x: " + getX(p1))
  println("Sommo due point p1 => " + p1 + " e p2 => " + p2 + " = " + sum(p1, p2))

}

object Sealed extends App {

  object PersonModule {
    sealed trait Person
    case class Student(name: String, year: Int) extends Person
    case class Teacher(name: String, course: String) extends Person

    def name(p: Person): String = p match {
      case Student(n, _) => n
      case Teacher(n, _) => n
      case _ => throw new IllegalArgumentException
    }
  }
  import PersonModule._
  println("Print the person: " + Teacher("Tonio", "Informatica"))

  sealed trait IntList
  case class ILNode(head: Int, tail: IntList) extends IntList
  case class ILNil() extends IntList
   //sum all the list element
  def sum(l: IntList): Int = {
    @annotation.tailrec
    def _sum(l: IntList, acc: Int): Int = l match {
      case ILNode(h, t) => _sum(t, h + acc)
      case _ => acc
    }
    _sum(l, 0)
  }
  println("Proviamo a stampare sta lista: " + sum(ILNode(10, ILNode(30, ILNode(50, ILNil())))))
}