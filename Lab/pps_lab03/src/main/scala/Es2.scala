object Negfunction extends App {
    val neg: (String => Boolean) => String => Boolean = f => g => !f(g)
    val negAlternativa: (String => Boolean) => String => Boolean = _ andThen(!_)
    def negFunction(f: String => Boolean): String => Boolean = x => !f(x)

    val notEmpty: (String => Boolean) = neg(x => x equals "")

    println("Ora passo una stringa che non è vuota e vediamo se da true:  " + neg(_ equals "").apply("diocane"))
    println("Ora passo una stringa che non è vuota e vediamo se da true:  " + negFunction(_ equals "ciao").apply("ciao"))
}

object parity extends App {
    val parity: Int => String = x => x match {
        case x if x % 2 != 0 => "Even"
        case _ => "Odd"
    }
    def parityFunction(x: Int): String = x match {
        case x if x % 2 != 0 => "Even"
        case _ => "Odd"
    }
    println("Prova di parity lambda: " + parity(3) + " e function: " + parityFunction(4))
}

object curryng extends App {
    val p1: (Int) => (Int) => (Int) => Boolean = x => y => z => (x, y, z) match {
        case (x, y, z) if x < y && y < z  => true
        case _ => false
    }
    val p2: (Int, Int, Int) =>  Boolean = (x, y, z) => (x, y, z) match {
        case (x, y, z) if x < y && y < z  => true
        case _ => false
    }
    def p3(x: Int)(y: Int)(z: Int): Boolean = (x, y, z) match {
        case (x, y, z) if x < y && y < z  => true
        case _ => false
    }
    def p4(x: Int, y: Int, z: Int): Boolean = (x, y, z) match {
        case (x, y, z) if x < y && y < z  => true
        case _ => false
    }

    println("Predicato curried lambda: " + p1(1)(2)(3))
    println("Predicato non curried lambda: " + p2(1, 2, 3))
    println("Predicato curried function: " + p3(1)(2)(3))
    println("Predicato non curried function: " + p4(1, 2, 3))
}

object composition extends App {
    def compose(f: Int => Int, g: Int => Int): Int => Int = x => f(g(x))
    println("Compongo delle funzioni: " + compose(_ - 1, _ *2).apply(2))
}

object fibonacci extends App {
    def fib(n: Int): Int = {
        def _fib(n: Int, acc: Int): Int = n match {
            case 0 => acc
            case 1 => acc + 1
            case n if n > 1 => _fib(n - 1, acc) + _fib(n - 2, acc)
        }
        _fib(n, 0)
    }

    def tailFib(n: Int): Int = {
        @annotation.tailrec
        def _fib2(a: Int, b: Int, n: Int): Int = n match {
            case 0 => a //quando arrivo alla fine metto acc1
            case _ => _fib2(b, a + b, n - 1) //a e b sono il prec e il prec2 che vengono fatti avanzare
        }
        _fib2(0, 1, n)
    }
    println("Fibonacci di 4: " + tailFib(4))

}
