object ToToString extends App{
  sealed trait Person
  case class Student(name: String, year: Int) extends Person
  case class Teacher(name: String, course: String) extends Person

  def personToString(p: Person): String = p match {
    case Student(n, y) => "\nStudent: \n name: " + n + "\n year: " + y
    case Teacher(n, c) => "\nTeacher: \n name: " + n + "\n course: " + c
  }

  val s: Student = Student("Mario", 80)
  val t: Teacher = Teacher("Viroli", "Sviluppo")

  println(personToString(s))
  println(personToString(t))
}

object Generics extends App {
  sealed trait List[E]

  case class Node[E](head: E, tail: List[E]) extends List[E]
  case class Nil[E]() extends List[E]

  def sum(l: List[Int]): Int = {
    @annotation.tailrec
    def _sum(l: List[Int], acc: Int): Int = l match {
      case Node(h, t) => _sum(t, h + acc)
      case _ => acc
    }
    _sum(l, 0)
  }

  def append[E](l1: List[E], l2: List[E]): List[E] = (l1, l2) match {
    case (Node(h, t), l2) => Node(h, append(t, l2))   //1^
    case (l1, Node(h, t)) => Node(h, append(l1, t)) //2^
    case _ => Nil()                                 //3^
    //Pattern node è per dire != NUll. Quando non è null il primo fai 1^ altrimenti 2^ e alla fine 3^
    /*
      l1 = (10, null), l2 = (20, (30, null))
      append(l1, l2)
      1^ Node(10, append(null, l2)
                  2^ Node(20, append(null, (30, null)
                              2^ Node(30, append(null, null)
     */
  }

  def drop[E](l: List[E], n: Int): List[E] = (l, n) match {
    case (Node(h, t), n) if n > 1 => drop(t, n -1)
    case (Node(h, t), 1) => t
  }

  def map[A, B](l: List[A]) (f: A => B): List[B] = l match {
    case Node(h, t) => Node[B](f(h), map(t)(f))
    case _ => Nil[B]()
  }

  def filter[E](l: List[E]) (f: E => Boolean): List[E] = l match {
    case Node(h, t) if f(h) => Node[E](h, filter(t)(f))
    case Node(h, t) if !f(h) => filter(t)(f)
    case Nil() => Nil[E]()
  }

  def max(l: List[Int]): Option[Int] = {
    def _max(l: List[Int], max: Int): Option[Int] = l match {
      case Node(h, t) if h > max => _max(t, h)
      case _ if max == -1 => None
      case _ => Option(max)
    }
    _max(l, -1)
  }
  println(sum(Node(10, Node(20, Node(30, Nil())))))
  println(append(Node(10, Node(20, Nil())), Node(20, Node(30, Nil()))))
  println(drop(Node(10, Node(20, Node(30, Nil()))),2))
  println(map(Node(10, Node(20, Nil())))(_+1))
  println(map(Node(10, Node(20, Nil())))(":"+_+":"))
  println(filter(Node(10, Node(20, Nil())))(_>10))
  println(filter(Node("a", Node("bb", Node("ccc", Nil()))))( _.length <=2))
  println(max(Node(10, Node(25, Node(20, Nil())))))
  println(max(Nil()))
}

import ToToString._
import Generics._
object extraTaskI extends App {
  def teacherCourse(l: List[Person]): List[String] = map(
    filter(l) ({
      case Teacher(_,_) => true
      case _ => false
    })
  )({
    case Teacher(_, course) => course
    case _ => ""
  })

  println(teacherCourse(Node(Teacher("gino", "infor"), Node(Teacher("bino", "math"), Node(Student("Luca", 20), Nil())))))

  def genOp(o: (Int, Int) => Int, i: Int): Int => Int = x => o(x, i)
  val minus3: Int => Int  = genOp(_ - _, 3)
  val div2: Int => Int = genOp(_ / _, 2)
  println("Composizione: " + (div2 compose minus3)(30))

  val incBy: Int => Int => Int = x =>  genOp(_ + _, x)
  val plus7 = incBy(7)
  println(plus7(8))

}

object fold extends App {
  def foldLeft(l: List[Int])(initVal: Int)(b: (Int, Int) => Int): Int = {
    def _foldLeft(l: List[Int], acc: Int):Int = l match {
      case Node(h, t) => _foldLeft(t, b(acc, h))
      case _ => acc
    }
    _foldLeft(l, initVal)
  }

  def foldRight(l: List[Int])(initVal: Int)(f: (Int, Int) => Int): Int = l match {
      case Node(h, t) => f(h, foldRight(t)(initVal)(f))
      case Nil() => initVal
  }

  val lst = Node(3,Node(7,Node(1,Node(5, Nil()))))
  println(foldLeft(lst)(0)(_-_))
  println(foldRight(lst)(0)(_-_))
}





